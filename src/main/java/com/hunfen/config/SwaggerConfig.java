package com.hunfen.config;



import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;


@EnableSwagger2     //开启swagger,访问地址 localhost:8080/swagger-ui.html
@Configuration
public class SwaggerConfig {
    private ApiInfo apiInfo() {
        Contact contact = new Contact("HongZeBin", "", "30317060@qq.com");
        return new ApiInfo("后端Api文档",
                "财务管理小程序的后端Api文档",
                "1.0",
                "urn:tos",
                contact,
                "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList());
    }
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hunfen.controller"))
                .build();
    }
}
