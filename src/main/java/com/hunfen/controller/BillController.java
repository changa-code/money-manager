package com.hunfen.controller;

import com.hunfen.common.Result;
import com.hunfen.pojo.Bill;
import com.hunfen.pojo.Sum;
import com.hunfen.service.impl.BillServiceImpl;
import com.hunfen.service.impl.UserServiceImpl;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/bill")
public class BillController {
    @Autowired
    BillServiceImpl billService;
    @ApiOperation("根据用户id获取按时间降序的账单")
    @GetMapping("/getBillsSortedByTime/{userId}")
    public Result getBillsSortedByTime(@PathVariable("userId")Long userId){
        List<Bill> billList = billService.getAllBillsSortedByTime(userId);
        if(billList.size() == 0){
            return Result.error("根据时间降序获取账单列表失败");
        }else{
            return Result.success("根据时间降序获取账单列表成功",billList);
        }
    }
    @ApiOperation("根据用户id获取年份收入账单")
    @GetMapping("/getReceiveSumInYear/{userId}")
    public Result getReceiveSumInYear(@PathVariable("userId")Long userId){
        List<Sum> sumList = billService.getReceiveSumOrderByYear(userId);
        if(sumList == null){
            return Result.error("获取年份收入账单列表失败");
        }else{
            return Result.success("获取年份收入账单列表成功",sumList);
        }
    }
    @ApiOperation("根据用户id获取年份支出账单")
    @GetMapping("/getSendSumInYear/{userId}")
    public Result getSendSumInYear(@PathVariable("userId")Long userId){
        List<Sum> sumList = billService.getSendSumOrderByYear(userId);
        if(sumList == null){
            return Result.error("获取年份支出账单列表失败");
        }else{
            return Result.success("获取年份支出账单列表成功",sumList);
        }
    }
    @ApiOperation("根据用户id获取月份收入账单")
    @GetMapping("/getReceiveSumInMonth/{userId}")
    public Result getReceiveSumInMonth(@PathVariable("userId")Long userId){
        List<Sum> sumList = billService.getReceiveSumOrderByMonth(userId);
        if(sumList == null){
            return Result.error("获取月份收入账单列表失败");
        }else{
            return Result.success("获取月份收入账单列表成功",sumList);
        }
    }
    @ApiOperation("根据用户id获取月份支出账单")
    @GetMapping("/getSendSumInMonth/{userId}")
    public Result getSendSumInMonth(@PathVariable("userId")Long userId){
        List<Sum> sumList = billService.getSendSumOrderByMonth(userId);
        if(sumList == null){
            return Result.error("获取月份支出账单列表失败");
        }else{
            return Result.success("获取月份支出账单列表成功",sumList);
        }
    }
    @GetMapping("/getBillById/{id}")
    public Result getBillById(@PathVariable("id")int id){
        Bill bill = billService.getBillById(id);
        if(bill != null){
            return Result.success("获取成功",bill);
        }else{
            return Result.error("获取失败");
        }
    }
    @PostMapping("/addSendBill")
    public Result addSendBill(@RequestBody Bill bill){
        bill.setReceiveOrSpend(0);  //0表示支出
        boolean b = billService.addBill(bill);
        if(b){
            return Result.success("添加成功",b);
        }else{
            return Result.error("添加失败");
        }
    }
    @PostMapping("/addReceiveBill")
    public Result addReceiveBill(@RequestBody Bill bill){
        bill.setReceiveOrSpend(1);  //1表示收入
        boolean b = billService.addBill(bill);
        if(b){
            return Result.success("添加成功",b);
        }else{
            return Result.error("添加失败");
        }
    }
    @PutMapping("/updateBillById")
    public Result updateBillById(@RequestBody Bill bill){
        boolean b = billService.updateBill(bill);
        if(b){
            return Result.success("修改成功",b);
        }else{
            return Result.error("修改失败");
        }
    }
    @DeleteMapping("/deleteBillById/{id}")
    public Result deleteBillById(@PathVariable int id){
        boolean b = billService.deleteBillById(id);
        if(b){
            return Result.success("删除成功",b);
        }else{
            return Result.error("删除失败");
        }
    }

}
