package com.hunfen.controller;

import com.hunfen.common.Result;
import com.hunfen.pojo.User;
import com.hunfen.service.impl.UserServiceImpl;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserServiceImpl userService;
    @GetMapping("/getUserList")
    public List<User> getUserList(){
        return userService.getUserList();
    }

    @GetMapping("/getUserById/{id}")
    public Result getUserById(@PathVariable("id")Long id){
        User userByID = userService.getUserById(id);
        if (userByID != null) {
            return Result.success("根据ID获得用户信息成功", userByID);
        } else {
            return Result.error("根据ID获得用户信息失败");
        }
    }

    @PutMapping("/updateUser")
    public Result updateUser(@RequestBody User user){
        boolean b = userService.addUser(user);
        if(b){
            return Result.success("修改成功",b);
        }else{
            return Result.error("修改失败");
        }
    }
}
