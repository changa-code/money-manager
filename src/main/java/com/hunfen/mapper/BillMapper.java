package com.hunfen.mapper;


import com.hunfen.pojo.Bill;
import com.hunfen.pojo.Sum;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Mapper
@Repository
public interface BillMapper {
    /**
     * 根据Id获得记录
     * @param id
     * @return
     */
    public Bill getBillById(@Param("id") int id);

    /**
     * 根据时间获得数据
     * @param date
     * @return
     */
    public List<Bill> getBillByTime(@Param("time")Date date);

    /**
     * 添加一条收入信息
     * @param bill
     * @return
     */
    public boolean addBill(Bill bill);

    /**
     * 根据ID删除一条记录
     * 说是删除记录其实是将isExist设置为0
     * @param id
     * @return
     */
    public boolean deleteBillById(@Param("id") int id);

    /**
     * 修改一条收入数据
     * @param bill
     * @return
     */
    public boolean updateBill(Bill bill);

    /**
     * 根据time降序排序账单
     * @return
     */
    public List<Bill> getAllBillsSortedByTime(Long userId);

    /**
     * 获取每一年及其收入金额
     * @return
     */
    public List<Sum> getReceiveSumOrderByYear(Long userId);
    /**
     * 获取每一年及其支出金额
     * @return
     */
    public List<Sum> getSendSumOrderByYear(Long userId);
    /**
     * 获取每一年每月份及其收入金额
     * @return
     */
    public List<Sum> getReceiveSumOrderByMonth(Long userId);
    /**
     * 获取每一年每月份及其支出金额
     * @return
     */
    public List<Sum> getSendSumOrderByMonth(Long userId);

}
