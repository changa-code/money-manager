package com.hunfen.mapper;

import com.hunfen.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Mapper
@Repository
public interface UserMapper {
    //获取用户列表
    List<User> getUserList();

    /**
    * 根据用户名查询用户信息
     * @param username 传入的参数：用户名
     * @return
     */
    User getUserByUsername(@Param("username") String username);

    /**
     * 传入一个用户的详细信息（不需要设置ID）
     * 返回的结果是一的情况代表创建用户成功
     * 返回结果之后原先传入的user变量可以直接通过getID获取她插入之后的主键ID
     * 获取主键ID之后可以进一步获取
     * @param user
     * @return
     */
    boolean addUser(User user);

    /**
     * 传入一个用户
     * 更新这个用户的信息（根据ID匹配）
     * @param user
     * @return
     */
    boolean updateUser(User user);

    /**
     * 根据id获取用户
     * @param id
     * @return
     */
    User getUserById(Long id);
}
