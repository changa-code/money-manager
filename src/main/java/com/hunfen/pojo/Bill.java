package com.hunfen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Bill {
    private int id;
    private int money;
    private int userId;
    private Date time;
    private String details;
    private int type;
    private int isExist;
    private int receiveOrSpend;
}
