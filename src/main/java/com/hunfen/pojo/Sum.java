package com.hunfen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sum {
    int id;
    int year;
    int month;
    int day;
    int count;  //总条数
    int sum;    //总金额
}
