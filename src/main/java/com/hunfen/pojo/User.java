package com.hunfen.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private int id;
    private String name;
    private int sex;
    private int age;
    private String username;
    private String password;
    private Date createTime;
    private String email;
    private String avatar;
}
