package com.hunfen.service;

import com.hunfen.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {
    List<User> getUserList();

    User getUserByUsername(String username);

    boolean addUser(User user);

    boolean updateUser(User user);

    User getUserById(Long id);
}
