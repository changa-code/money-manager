package com.hunfen.service.impl;

import com.hunfen.mapper.BillMapper;
import com.hunfen.pojo.Bill;
import com.hunfen.pojo.Sum;
import com.hunfen.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class BillServiceImpl implements BillService {
    @Autowired
    private BillMapper billMapper;


    @Override
    public Bill getBillById(int id) {
        return billMapper.getBillById(id);
    }

    @Override
    public List<Bill> getBillByTime(Date date) {
        return billMapper.getBillByTime(date);
    }

    @Override
    public boolean addBill(Bill bill) {
        return billMapper.addBill(bill);
    }

    @Override
    public boolean deleteBillById(int id) {
        return billMapper.deleteBillById(id);
    }

    @Override
    public boolean updateBill(Bill bill) {
        return billMapper.updateBill(bill);
    }

    @Override
    public List<Bill> getAllBillsSortedByTime(Long userId) {
        return billMapper.getAllBillsSortedByTime(userId);
    }

    @Override
    public List<Sum> getReceiveSumOrderByYear(Long userId) {
        return billMapper.getReceiveSumOrderByYear(userId);
    }

    @Override
    public List<Sum> getSendSumOrderByYear(Long userId) {
        return billMapper.getSendSumOrderByYear(userId);
    }

    @Override
    public List<Sum> getReceiveSumOrderByMonth(Long userId) {
        return billMapper.getReceiveSumOrderByMonth(userId);
    }

    @Override
    public List<Sum> getSendSumOrderByMonth(Long userId) {
        return billMapper.getSendSumOrderByMonth(userId);
    }


}
