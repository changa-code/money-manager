package com.hunfen;

import com.hunfen.pojo.User;
import com.hunfen.service.impl.UserServiceImpl;
import com.hunfen.utils.JwtTokenUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
class MoneyManagerApplicationTests {
    @Test
    void contextLoads() {
        String jwt = JwtTokenUtils.generateToken("hzb");
        System.out.println(jwt);
    }

}
